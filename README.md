# Threat hunting like a gutter punk
# Intro
# What’s a gutter punk
Show pictures of gutter punks and their different names

Crusties
Oogles
Punk Hobos
“Travelers”

*Why is it relevant?*  

Resiliency breeds Creativity. 

- Show “Down and Out In New Orleans” book and talk about it


# Common pitfalls with security programs
* Your CIO is also your CSO

Conflicting interests. Constant fight for resources. 

* No security product is going to be a silver bullet. 

Too much focus on buying best of breed to just “Set it and forget it”. Can’t tell you how many times I’ve heard ‘Well we looked at some kind of magic chart and picked the thing that was most top-right’

* Your security team wears multiple hats in the org

If your security people are spread thin on other tasks, the motivation to improve the security program takes a back burner. 

* Reactive approaches to security have been proven to not work. So why keep doing it? 

 Enter Threat Hunting


# Demystifying Threat Hunting
* Threat Hunting is a proactive methodology of implementing “Continuous Improvement” (aka CI) procedures within a security operations that developers and engineers have been using for decades. 

- Tell story about becoming a TH at Foreground (later bought out by Ray)
	- After a couple weeks on the job realized it was just another security analysis job, but different. (Show The interview meme about ‘but different’) https://thumbs.gfycat.com/FoolishThornyDunnart-size_restricted.gif

However, the big difference was I didn’t get bored with the job. 
* Being encouraged to get creative means employees are able to flex skills and improve on them while ultimately helping the client. It’s essentially free training for the passionate gutter punk. 

# Make fun of the threat cycles
- Show as many different iterations of a vendor supplied cycle. 

- I feel like vendors put a lot of thought into what happens during a hunt when most companies struggle with just starting one. 

# So you’re a gutter punk, how do you revamp or start a TH program? 
Most programs have to make one of two decisions when they start out. 
* Use the tools you already have?
* Or build the tools

I always suggest using the tools you already have if you can make it work. But a couple reasons why you might want to consider building your own:

* You have too many tools that don’t congeal with each other. A analyst has to enter each tool to relate data to each other (aka correlation). 

This is a major drag on the analyst and hampers or straight up prevents pivoting between data sources in order to quickly gather the ‘big picture’. 

* The wisdom of “Whatever you don’t spend on tools, you spend on labor” is being eroded. 

Security people embrace open source and contribute back into it. Hiring a passionate gutter punk won’t break the bank, but a Netwitness cluster will. 



# Lets look at some SIEMs
* SIEM - Security Information and Event Management

So what does that mean? 

* “Collect, Analyze, and Alert”

Coming from Raytheon I worked with commercial SIEMs mainly: Netwitness, Splunk ES, LogRhythm, etc. I did not get assigned any ELK clients, but we had some. I left Raytheon to join a startup where we specialize in standing up virtual ranges with open source tools. These ranges are used to train/assess individuals or teams. 

So I had to quickly learn ELK. Easy right? 

Having seen so many SIEMs I had certain expectations. After playing around in it I assessed vanilla ELK isn’t a SIEM . It seemed like too much work to make it a practical SIEM comparable to commercial products.  I started looking at security specific implementations of ELK to save me time: 

* HELK - A Hunting ELK (Elasticsearch, Logstash, Kibana) with advanced analytic capabilities.

* SOF-ELK - Platform was developed for SANS to help teach SIEM related courses. Distributed as a free and open source resource for the community at large, without a specific course requirement or tie-in required to use it.

* Mozdef - The Mozilla Enterprise Defense Platform seeks to automate the security incident handling process and facilitate the real-time activities of incident handlers.

* RockNSM - Created by the Missouri National Guard Cyber Team. The combination of tools allows the National Guard to set up their data collection for security monitoring and incident response in 20 minutes.

* Security Onion - Security Onion is a free and open source Linux distribution for intrusion detection, enterprise security monitoring, and log management. 

My big take away here is all these projects chosen ELK and changed it. Maybe I should revisit plain ELK. 


# So it looks like ELK is dominating. Lets try that. 
I’m not going to spend 10 slides on setting up ELK or go in-depth into each ELK feature, but if you’re not familiar here’s a quick breakdown:

* Released in 2010 by Shay Banon to create a scalable search solution for big data. Consists of three components that makes up the acronym (Also known as the Elastic Stack)

* Elasticsearch - Search engine that provides scalable search, has near real-time results and supports multitenancy. 
* Logstash - Data collection and log-parsing engine
* Kibana - Analytics and visualization platform


# Use Cases
Here I’m going to show you some use cases/hunts that utilize ELK to find common things most orgs will face. 

Before starting a TH program you need to define the following:
* What are my threats and risks? 
	* Just because you are in a particular vertical doesn’t mean you all share the same threats/risks. 
* What data sources can I use to detect those threats? 
	* Really put some thought into this. Too many orgs dump everything they have into their SIEM without considering their threats in the hope to catch the ‘unknown threat’. Careful configuration of log management/filtering in the beginning will save hours of tuning and slow searches breaking your gutter punk spirit. 

Stuff from the phd paper: https://www.syspanda.com/index.php/2018/11/26/developing-adaptive-threat-hunting-solution-elasticsearch-stack-masters-thesis/








